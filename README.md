How to use Docker to dockerize a React app, Nodejs and Nginx reverse proxy - step by step tutorial


### How to run

docker-compose -f .\docker-compose.yaml up --build

### Api endpoint:

http://localhost:8001/api

### Front-end:

http://localhost:3001

http://localhost:9001